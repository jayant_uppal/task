package com.test.study.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_id")
    private Long id;

    @Column(name = "student_name")
    private String name;

    @ManyToMany( cascade = { CascadeType.ALL })
    @JoinTable( name = "students_courses",
                joinColumns = {
                        @JoinColumn(name = "student_id"),
                },
                inverseJoinColumns = {
                        @JoinColumn(name = "course_id")
                })
    private Set<Course> courses = new HashSet<Course>();

    public Student() {
    }

    public Student(String name) {
        this.name = name;
    }

    public Student(String name, Set<Course> courses) {
        this.name = name;
        this.courses = courses;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }
}
