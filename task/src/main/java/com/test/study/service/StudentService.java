package com.test.study.service;

import com.test.study.models.Course;
import com.test.study.models.Student;
import com.test.study.repos.CourseRepo;
import com.test.study.repos.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    StudentRepo studentRepo;

    @Autowired
    CourseRepo courseRepo;

    public Student getStudentById(Long id) throws Exception{
        Optional<Student> current = studentRepo.findById(id);
        if (current.isPresent()) {
            return current.get();
        }
        else {
            throw new Exception("Student not found");
        }
    }

    public List<Student> getAllStudents() {
        return studentRepo.findAll();
    }

    public Student addStudent(Student student) {
        Student updatedStudent = new Student();

        updatedStudent.setName(student.getName());
        addStudentCourse(student, updatedStudent);

        return studentRepo.save(updatedStudent);
    }

    public Student updateStudent(Long id, Student student) throws Exception{
        Optional<Student> current = studentRepo.findById(id);
        if (current.isPresent()) {
            Student currentStudent = current.get();

            currentStudent.setName(student.getName());
            addStudentCourse(student, currentStudent);

            return studentRepo.save(currentStudent);
        }
        else {
            throw new Exception("Student not found");
        }
    }

    public void deleteStudent(Long id) throws Exception{
        Optional<Student> current = studentRepo.findById(id);
        if (current.isPresent()) {
            // Remove all relationships
            Student student = current.get();
            for (Course course: new HashSet<Course>(student.getCourses())) {
                student.getCourses().remove(course);
                course.getStudents().remove(student);
            }
            studentRepo.deleteById(id);
        }
        else {
            throw new Exception("Student not found");
        }
    }

    private void addStudentCourse(Student student, Student updatedStudent) {
        updatedStudent.setCourses(new HashSet<Course>());

        student.getCourses().stream().forEach(course -> {
            Course currentCourse = courseRepo.findByName(course.getName());

            if (currentCourse == null) { // No such course is available
                Course newCourse = new Course();
                newCourse.setName(course.getName());

                updatedStudent.getCourses().add(newCourse);
                newCourse.getStudents().add(updatedStudent);
            }
            else { // Same course is available
                updatedStudent.getCourses().add(currentCourse);
                currentCourse.getStudents().add(updatedStudent);
            }
        });
    }
}
