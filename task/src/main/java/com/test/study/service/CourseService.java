package com.test.study.service;

import com.test.study.models.Course;
import com.test.study.models.Student;
import com.test.study.repos.CourseRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseService {

    @Autowired
    CourseRepo courseRepo;

    public Course getCourseByName(String name) throws Exception{
        Course current = courseRepo.findByName(name);
        if (current != null) {
            return current;
        }
        else {
            throw new Exception("Course not found");
        }
    }

    public List<Course> getAllCourses() {
        return courseRepo.findAll();
    }

    public Course addCourse(Course course) {
        return courseRepo.save(course);
    }
}
