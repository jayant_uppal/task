package com.test.study.controller;

import com.test.study.models.Course;
import com.test.study.models.Student;
import com.test.study.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/onboarding/v1/")
public class CourseController {

    @Autowired
    CourseService courseService;

    @GetMapping("/course/{name}")
    public ResponseEntity<Course> getCourseByName(@PathVariable("name") String name) {
        try {
            return new ResponseEntity<>(courseService.getCourseByName(name), HttpStatus.OK);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/course")
    public ResponseEntity<List<Course>> getAllCourses() {
        try {
            return new ResponseEntity<>(courseService.getAllCourses(), HttpStatus.OK);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/course")
    public ResponseEntity<Course> addCourse(@RequestBody Course course) {
        try {
            return new ResponseEntity<>(courseService.addCourse(course), HttpStatus.CREATED);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
